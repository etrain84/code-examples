<?php 

//Dynamically build a list of links
//from a given csv file
//identifying / sensitive elements replaced with {VARIABLE}

    //array used for new key names later
    $fields = [
        'customer',
        'A',
        'B',
        'C',
        'D',
        'E',
        'F',
        'G'
    ];

    $linkSource = [];
    $linkList = [];

    //open and read the csv file
    //build an array of csv data for use later
    $row = 1;
    if (($handle = fopen("{CSV}", "r")) !== FALSE) {
        while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
            $data = array_combine($fields,$data);
            array_push($linkSource,$data);
        }
        fclose($handle);
    }

    //use built array to build link list
    //href to conform to known route
    foreach($linkSource as $customer) {
        $url = "{URL}";
        $str = '';
        $hostingId = 'x';
        foreach($customer as $key => $option) {
            
            if($option !== '') {
                switch($key) {
                    case 'customer':
                        $str .= $option." - <a style='color:blue;' href='".$url.urlencode($option);
                        break;
                    case '{URLPARAM}':
                        if($option !== '') {
                            $str .= "{URLPARAM}/";
                            $str .= $key."|";
                        }
                        break;
                    case '{URLPARAMTWO}':
                        if($option !== '') {
                            $str .= "{URLPARAMTWO}/";
                            $str .= $key."|";
                        }
                        break;
                    case 'id':
                        $hostingId = $option;
                        break;
                    default :
                        if($option !== 'x') {
                            $str .= $key.":".$option."|";
                        } else {
                            $str .= $key."|";
                        }
                        break;
                }
            }
        }
        $str .= "/".$hostingId."'>Review Link</a>";
        array_push($linkList, $str);
    }

    return $linkList;

?>
